# CMake generated Testfile for 
# Source directory: /home/pokpong/catkin_ws/src
# Build directory: /home/pokpong/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("openseal_console")
subdirs("openseal_viz")
subdirs("openseal_description")
subdirs("openseal_llc")
