#!/usr/bin/env python
import cv2
import numpy as np
import time

height = 1080
width = 1080

DIM = (width, height)

K=np.array([[439.58915707325974, 0.0, 529.7177693267203], [0.0, 589.4347706646346, 418.6259830785021], [0.0, 0.0, 1.0]])
D=np.array([[3.7112375714156536e-05], [-0.09163571828314654], [-0.02686673222241123], [0.04079084026232999]])

def gstreamer_pipeline (capture_width=width, capture_height=height, display_width=width, display_height=height, framerate=60, flip_method=0) :   
    return ('nvarguscamerasrc ! ' 
    'video/x-raw(memory:NVMM), '
    'width=(int)%d, height=(int)%d, '
    'format=(string)NV12, framerate=(fraction)%d/1 ! '
    'nvvidconv flip-method=%d ! '
    'video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! '
    'videoconvert ! '
    'video/x-raw, format=(string)BGR ! appsink'  % (capture_width,capture_height,framerate,flip_method,display_width,display_height))

def undistort_ext(img, balance=0.0, dim2=None, dim3=None):
    # undistort_ext(img, 0.7, (230,230), (2000,2000))
    dim1 = img.shape[:2][::-1]  
    assert dim1[0]/dim1[1] == DIM[0]/DIM[1], "Image to undistort needs to have same aspect ratio as the ones used in calibration"
    if not dim2:
        dim2 = dim1
    if not dim3:
        dim3 = dim1
    scaled_K = K * dim1[0] / DIM[0]  # The values of K is to scale with image dimension.
    scaled_K[2][2] = 1.0  # Except that K[2][2] is always 1.0
    # This is how scaled_K, dim2 and balance are used to determine the final K used to un-distort image. OpenCV document failed to make this clear!
    new_K = cv2.fisheye.estimateNewCameraMatrixForUndistortRectify(scaled_K, D, dim2, np.eye(3), balance=balance)
    map1, map2 = cv2.fisheye.initUndistortRectifyMap(scaled_K, D, np.eye(3), new_K, dim3, cv2.CV_16SC2)
    undistorted_img = cv2.remap(img, map1, map2, interpolation=cv2.INTER_LINEAR, borderMode=cv2.BORDER_CONSTANT)
    crop_img = undistorted_img[500:1350, 280:1300]
    cv2.imshow("CSI Camera", crop_img)

def show_camera():
    print (gstreamer_pipeline())
    cap = cv2.VideoCapture(gstreamer_pipeline(), cv2.CAP_GSTREAMER)
    if cap.isOpened():
        window_handle = cv2.namedWindow('CSI Camera', cv2.WINDOW_AUTOSIZE)
        map1, map2 = cv2.fisheye.initUndistortRectifyMap(K, D, np.eye(3), K, DIM, cv2.CV_16SC2)
        while cv2.getWindowProperty('CSI Camera',0) >= 0:
            start_time = time.time()
            ret_val, img = cap.read()
            h,w = img.shape[:2]
            undistorted_img = cv2.remap(img, map1, map2, interpolation=cv2.INTER_LINEAR, borderMode=cv2.BORDER_CONSTANT)
            cv2.imshow("CSI Camera", undistorted_img)
            # print ("FPS: ", 1.0 / (time.time() - start_time))
            keyCode = cv2.waitKey(30) & 0xff
            # Stop the program on the ESC key
            if keyCode == 27:
               break
        cap.release()
        cv2.destroyAllWindows()
    else:
        print 'Unable to open camera'


if __name__ == '__main__':
    show_camera()
