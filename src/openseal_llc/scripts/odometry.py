#!/usr/bin/env python
import math
import rospy
from nav_msgs.msg import Odometry
import geometry_msgs.msg
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, Vector3
from sensor_msgs.msg import JointState

rospy.init_node('odometry_publisher')

odom_pub = rospy.Publisher("odom", Odometry, queue_size=50)


L = 0.2
R = 0.03225

current_time = rospy.Time.now()
last_time = rospy.Time.now()

wl = 0
wr = 0
vx = 0
wz = 0
x_est = 0
y_est = 0
yaw_est = 0


def odom_cb(data):
    global yaw_est, x_est, y_est

    current_time = rospy.Time.now()

    wl = data.velocity[0]
    wr = data.velocity[1]
    vl = wl*R
    vr = wr*R
    vx = (0.5)*(vr+vl)
    wz = (1/L)*(vr-vl)

    
    dt = (current_time - last_time).to_sec()
    yaw_est += dt*wz;   
    if yaw_est > 6.28319:
        yaw_est -= 6.28319
    if yaw_est < 0:
        yaw_est += 6.28319

    x_dot = vx*math.cos(yaw_est)
    y_dot = vx*math.sin(yaw_est)
    x_est += dt*x_dot
    y_est += dt*y_dot

    odom_quat = tf.transformations.quaternion_from_euler(0, 0, yaw_est)

    odom = Odometry()
    odom.header.stamp = current_time
    odom.header.frame_id = "odom"
    odom.pose.pose = Pose(Point(x_est-0.02987, y_est, 0.0635), Quaternion(*odom_quat))
    odom.child_frame_id = "base_link"
    odom.twist.twist.linear.x = vx
    odom.twist.twist.angular.z = wz
    odom_pub.publish(odom)



if __name__ == '__main__':
    try:
        rospy.Subscriber('wheel', JointState, odom_cb)
        rospy.spin()
    except rospy.ROSInterruptException:
        pass