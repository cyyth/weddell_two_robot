#!/usr/bin/env python
import rospy
from std_msgs.msg import Float64
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Joy

cmd_L_power_pub = rospy.Publisher('cmd_L_vel', Float64, queue_size=10)
cmd_R_power_pub = rospy.Publisher('cmd_R_vel', Float64, queue_size=10)
rospy.init_node('joy_teleop', anonymous=True)

L = 0.2;
R = 0.03225;

MAX_SPD = 1
MAX_ANG = 4.3

SEC_SPD = 0.2

def seperate_twist(lin_x, ang_z):
    c1 = (2*lin_x)/R #wr+wl
    c2 = (L*ang_z)/R #wr-wl
    wr = (c1+c2)/2 
    wl = c1-wr
    return (wl, wr)

def power_cmd(joy):
    rate = rospy.Rate(100)
    spin_pos = joy.buttons[4] 
    spin_neg = joy.buttons[5]
    max_spd = joy.buttons[6]
    throttle_factor = joy.axes[1]
    turn_factor = joy.axes[2]
    if max_spd:
        vx = MAX_SPD*throttle_factor
    else:
        vx = SEC_SPD*throttle_factor
    if not (spin_pos or spin_neg):
        wl, wr = seperate_twist(vx, 0)
        if turn_factor > 0:
            wl *= (1-turn_factor)
            wr = wr
        elif turn_factor < 0:
            wr *= (1+turn_factor)
            wl = wl
        else:
            pass
    else:
        if spin_pos:
            wl = -5
            wr = 5
        elif spin_neg:
            wl = 5
            wr = -5
    cmd_L_power_pub.publish(wl)
    cmd_R_power_pub.publish(wr)
    

 

def twist_to_diff():
    rospy.Subscriber('joy', Joy, power_cmd)
    rospy.spin()

if __name__ == '__main__':
    try:
        twist_to_diff()
    except rospy.ROSInterruptException:
        pass
