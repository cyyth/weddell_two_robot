#!/usr/bin/env python
import rospy
import math
from geometry_msgs.msg import Twist
from std_msgs.msg import Float64
from sensor_msgs.msg import JointState

cmd_L_vel_pub = rospy.Publisher('cmd_L_vel', Float64, queue_size=10)
cmd_R_vel_pub = rospy.Publisher('cmd_R_vel', Float64, queue_size=10)

L = 0.2
R = 0.03225

#max linx: +-0.43 max angz: +-4.3 
# def twist_convert():

rospy.init_node('kinematics')

def seperate_twist(lin_x, ang_z):
    c1 = (2*lin_x)/R #wr+wl
    c2 = (L*ang_z)/R #wr-wl
    wr = (c1+c2)/2 
    wl = c1-wr
    print (lin_x,ang_z,wl+1, wr+1)
    return (wl, wr)

def twist_cb(data):
    rate = rospy.Rate(10)
    l_pow, r_pow = seperate_twist(data.linear.x, data.angular.z)
    cmd_L_vel_pub.publish(l_pow+1)
    cmd_R_vel_pub.publish(r_pow+1)
    

if __name__ == '__main__':
    try:
        rospy.Subscriber('cmd_vel', Twist, twist_cb)
        rospy.spin()
    except rospy.ROSInterruptException:
        pass