#!/usr/bin/env python
import rospy
from std_msgs.msg import Float64
from geometry_msgs.msg import Twist

cmd_L_power_pub = rospy.Publisher('cmd_L_vel', Float64, queue_size=10)
cmd_R_power_pub = rospy.Publisher('cmd_R_vel', Float64, queue_size=10)
rospy.init_node('uni_to_diff', anonymous=True)

def power_cmd(data):
    rate = rospy.Rate(100)
    spd = 1
    if data.angular.z<0:
	l_pow = 3
	r_pow = -3
    elif data.angular.z>0:
	l_pow = -3
	r_pow = 3
    elif data.linear.x >0:
	l_pow = 5
	r_pow = 5
    elif data.linear.x <0:
	l_pow = -5
	r_pow = -5
    else:
	l_pow = 0
	r_pow = 0

    cmd_L_power_pub.publish(l_pow)
    st = "Time: {}	Left power: {}	Right power: {}".format(rospy.get_time(),l_pow, r_pow)
    rospy.loginfo(st)
    cmd_R_power_pub.publish(r_pow)
 

def twist_to_diff():
    rospy.Subscriber('key_vel', Twist, power_cmd)
    rospy.spin()

if __name__ == '__main__':
    try:
        twist_to_diff()
    except rospy.ROSInterruptException:
        pass
