#!/usr/bin/env python
import rospy
import math
from pid import PID
from geometry_msgs.msg import Twist
from std_msgs.msg import Float64
from sensor_msgs.msg import JointState
from actionlib_msgs.msg import GoalStatusArray

cmd_L_power_pub = rospy.Publisher('cmd_L_power', Float64, queue_size=10)
cmd_R_power_pub = rospy.Publisher('cmd_R_power', Float64, queue_size=10)

L = 0.2;
R = 0.03225;

#max linx: +-0.43 max angz: +-4.3 
# def twist_convert():
lp = 0.6
li = 10
ld = 0.001
l_controller = PID(lp*0.01,li*0.01,ld*0.01)
l_controller.setWindup(2.5)
l_controller.SetPoint = 0

rp = lp
ri = li
rd = ld
r_controller = PID(lp*0.01,li*0.01,ld*0.01)
r_controller.setWindup(2.5)
r_controller.SetPoint = 0

l_pow = 0
r_pow = 0
rospy.init_node('controller')

count = 0

def limit(x):
    if x>1:x=1
    elif x<-1:x=-1
    else: x=x
    return x

def left_setter_cb(setpoint):
    l_controller.SetPoint=setpoint.data

def right_setter_cb(setpoint):
    r_controller.SetPoint=setpoint.data

def left_wheel_vel_cb(data):
    rate = rospy.Rate(100)
    feedback = data.velocity[0]
    l_controller.update(feedback)
    cmd_L_power_pub.publish(limit(l_controller.output))
    

def right_wheel_vel_cb(data):
    rate = rospy.Rate(100)
    feedback = data.velocity[0]
    r_controller.update(feedback)
    cmd_R_power_pub.publish(limit(r_controller.output))

def reached_cb(status):
    global count
    if status.status_list:
        if status.status_list[0].status == 3 and count < 10:
            count += 1
            l_controller.SetPoint=0
            r_controller.SetPoint=0
        elif status.status_list[0].status == 1:
            count = 0
    else:
        pass

if __name__ == '__main__':
    try:
        rospy.Subscriber('wheel_vel/left', JointState, left_wheel_vel_cb)
        rospy.Subscriber('wheel_vel/right', JointState, right_wheel_vel_cb)
        rospy.Subscriber('cmd_L_vel', Float64, left_setter_cb)
        rospy.Subscriber('cmd_R_vel', Float64, right_setter_cb)
        rospy.Subscriber('move_base/status', GoalStatusArray, reached_cb)
        # print ("left_out: "+str(l_controller.SetPoint)+" "+str(l_controller.output)+"\t"+"right_out: "+str(r_controller.SetPoint)+" "+str(r_controller.output))
        
        rospy.spin()
    except rospy.ROSInterruptException:
        pass